PACKAGE_SRC = src

BUILD_ROOT=$(shell kpsewhich -var-value=TEXMFHOME)
BUILD_DIR = $(BUILD_ROOT)/tex/latex/posterUtcRoberval

install: src/beamerthemePOSTER_UTC.sty
	@echo $(BUILD_DIR)
	mkdir -p $(BUILD_DIR)
	cp -r $(PACKAGE_SRC)/* $(BUILD_DIR)
	texhash $(BUILD_ROOT)
demo:
	cd example && pdflatex \\nonstopmode\\input example.tex && pdflatex \\nonstopmode\\input example.tex
clean:
	rm -r $(BUILD_DIR)
	texhash $(BUILD_DIR)
